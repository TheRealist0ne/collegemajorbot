# /u/CollegeMajorBot

## About

[/u/CollegeMajorBot](www.reddit.com/u/CollegeMajorBot) is a reddit bot designed to help students, both incoming and
current, decide on a future career and major. Choosing your major is the most important thing you'll do in your college career. Unfortunately, many counselors are next to useless and often give students misleading or downright incorrect information.

This bot is here to help!

## Commands

**!majors**:

This command causes the bot to reply to the comment with a list of the top 200 college degrees. In case the user forgets what majors are listed, this is the command to use. Recommended to memorize majors you are interested in and use below command to summon bot.

Bot will also apply with below command and await user response.


**!info [major]**

The bot replies with the information about the given major. Bot will also reply with above command.


### Note

At the moment this bot is U.S focused. I am working on getting information for EU countries as well.
